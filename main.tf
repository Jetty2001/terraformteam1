terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1" 
}

terraform {
  backend "s3" {
    bucket = "techchak1cardealer1"
    key    = "techchak1cardealer1-TFstate"
    region = "us-east-1"
  }
}
resource "aws_s3_bucket" "techchak1cardealer" {
  bucket = "techchak1cardealer"
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
resource "aws_s3_bucket_acl" "techchak1cardealer" {
  bucket = aws_s3_bucket.techchak1cardealer.id
  acl    = "private"
}
locals {
  s3_origin_id = "myS3Origin"
}
resource "aws_s3_bucket_website_configuration" "techchak1cardealer" {
  bucket = aws_s3_bucket.techchak1cardealer.id
  index_document {
    suffix = "index.html"
  }
  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_policy" "s3_policy" {
  bucket = aws_s3_bucket.techchak1cardealer.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "Grant cloudfront access to the bucket"
        Effect = "Allow"
        Principal = {
          AWS = "*"
        }
        Action   = "s3:GetObject"
        Resource = "${aws_s3_bucket.techchak1cardealer.arn}/*"
      }
    ]
  })
}

resource "aws_s3_bucket_public_access_block" "team1bucket_public_block" {
  bucket = aws_s3_bucket.techchak1cardealer.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


resource "aws_cloudfront_origin_access_control" "default" {
  name                              = "example"
  description                       = "Example Policy"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}
resource "aws_cloudfront_distribution" "techchak1cardealer" {
  origin {
    domain_name              = aws_s3_bucket.techchak1cardealer.bucket_regional_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.default.id
    origin_id                = local.s3_origin_id
  }
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "team1"
  default_root_object = "index.html"

  # logging_config {
  #   include_cookies = false
  #   bucket          = "mylogs.s3.amazonaws.com"
  #   prefix          = "myprefix"
  # }

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }
  # Cache behavior with precedence 0
  ordered_cache_behavior {
    path_pattern     = "/content/immutable/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = local.s3_origin_id
    forwarded_values {
      query_string = false
      headers      = ["Origin"]
      cookies {
        forward = "none"
      }
    }
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }
  # Cache behavior with precedence 1
  ordered_cache_behavior {
    path_pattern     = "/content/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }
  price_class = "PriceClass_200"
  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }
  tags = {
    Environment = "dev"
  }
  viewer_certificate {
    cloudfront_default_certificate = true
  }
}
resource "aws_route53_zone" "primary" {
  name = "techchak1cardealer1.com"
}
resource "aws_route53_record" "root_domain" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "www.techchak1cardealer1.com"
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.techchak1cardealer.domain_name
    zone_id                = aws_cloudfront_distribution.techchak1cardealer.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_instance" "team1_server" {
  ami                     = "ami-006dcf34c09e50022"
  instance_type           = "t2.micro"
  key_name = "techchakkeypair"
  vpc_security_group_ids = [aws_security_group.team1_server_sg.id]
  subnet_id = aws_subnet.team1_public_az1.id
  associate_public_ip_address ="true"
  user_data     = <<-EOF
    #!/bin/bash
    sudo yum install -y gcc-c++ make
    curl -sL https://rpm.nodesource.com/setup_14.x | sudo -E bash -
    sudo yum install -y nodejs
    sudo npm install 
    npm install pm2@latest -g
    sudo yum install git -y
    git clone https://github.com/oyiz-michael/TechChak-Team1-Terraform-project.git
  EOF

  tags = {
    "Name" = "team1_server"
  }
}

# resource "aws_eip" "bar" {
#   vpc = true

#   instance                  = aws_instance.foo.id
#   associate_with_private_ip = "10.0.0.12"
#   depends_on                = [aws_internet_gateway.gw]
# }

resource "aws_vpc" "team1_vpc" {
    cidr_block = "10.0.0.0/16"
  tags = {
    Name = "team1_vpc"
  }
}

resource "aws_subnet" "team1_public_az1" {
  availability_zone = "us-east-1a"
  cidr_block = "10.0.1.0/24"
  vpc_id =  aws_vpc.team1_vpc.id
  tags = {
    Name = "team1_az1"
  }
}

resource "aws_subnet" "team1_public_az2" {
  availability_zone = "us-east-1b"
  cidr_block = "10.0.5.0/24"
  vpc_id = aws_vpc.team1_vpc.id
  tags = {
    Name = "team1_az2"
    }
}

resource "aws_route_table" "team1_rt" {
  vpc_id = aws_vpc.team1_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  
  tags = {
    Name = "team1_rt"
  }
}
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.team1_vpc.id
  tags = {
    Name = "team1_gw"
  }
}

resource "aws_route" "team1_publicrt" {
  route_table_id            = aws_route_table.team1_rt.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.gw.id
}

resource "aws_route_table_association" "team1_rta" {
  subnet_id      = aws_subnet.team1_public_az1.id
  route_table_id = aws_route_table.team1_rt.id
}

resource "aws_security_group" "team1_lb_sg" {
  name        = "team1_lb_sg"
  description = "team1_lb_sg inbound traffic"
  vpc_id      = aws_vpc.team1_vpc.id
  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_security_group" "team1_server_sg" {
  name        = "team1_server_sg"
  description = "team1_server_sg inbound traffic"
  vpc_id      = aws_vpc.team1_vpc.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 5000
    to_port          = 5000
    protocol         = "tcp"
    security_groups = [aws_security_group.team1_lb_sg.id]
  }

 ingress {
    description      = "SSH to EC2"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_"
  }
}

resource "aws_lb" "team1ALB" {
  name               = "team1ALB"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.team1_lb_sg.id]
  subnets            = [
    aws_subnet.team1_public_az1.id,
    aws_subnet.team1_public_az2.id
  ]

  enable_deletion_protection = true

  # access_logs {
  #   bucket  = aws_s3_bucket.techchak1cardealer.id
  #   prefix  = "test-lb"
  #   enabled = true
  # }

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.team1ALB.arn
  port              = 443
  protocol          = "HTTPS"
  certificate_arn = "arn:aws:acm:us-east-1:126552587903:certificate/b1bd4765-a20b-44f2-83a8-92ee98b25b2e"
 
  default_action {
    target_group_arn = aws_lb_target_group.test.arn
    type             = "forward"
}
}

resource "aws_lb_target_group" "test" {
  name     = "tf-example-lb-tg9"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.team1_vpc.id
}


